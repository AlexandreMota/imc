package com.alexandremota.imc.entity;

import java.io.Serializable;

/**
 * Created by Desenvolvimento on 27/07/2016.
 */
public class IMC implements Serializable {

    private String name;
    private String gender;
    private double height;
    private double weight;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String calculatorStatusIMC(){

        double valueIMC = calculatorValueIMC();

        String result = "";

        if(getGender().equals("Masculino")){
            result = manTable(valueIMC);
        }else if(getGender().equals("Feminino")){
            result = womanTable(valueIMC);
        }

        return result;
    }

    public double calculatorValueIMC(){
        double valueIMC = getWeight() / (getHeight() * getHeight());
        return valueIMC;
    }


    public String womanTable(double valueIMC){
        String result = "";

        if(valueIMC < 19.1){
            result = "Abaixo do peso";
        }else if(valueIMC >= 19.1 && valueIMC <= 25.8){
            result = "Peso ideal";
        }else if(valueIMC >= 25.9 && valueIMC <= 27.3){
            result = "Pouco acima do peso";
        }else if(valueIMC >= 27.4 && valueIMC <= 32.3){
            result = "Acima do peso";
        }else if(valueIMC >= 32.4 ){
            result = "Obesidade";
        }

        return result;
    }

    public String manTable(double valueIMC){
        String result = "";

        if(valueIMC < 20.7){
            result = "Abaixo do peso";
        }else if(valueIMC >= 20.7 && valueIMC <= 26.4){
            result = "Peso ideal";
        }else if(valueIMC >= 26.5 && valueIMC <= 27.8){
            result = "Pouco acima do peso";
        }else if(valueIMC >= 27.9 && valueIMC <= 31.1){
            result = "Acima do peso";
        }else if(valueIMC >= 31.2 ){
            result = "Obesidade";
        }

        return result;
    }
}
