package com.alexandremota.imc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alexandremota.imc.entity.IMC;


public class NameActivity extends AppCompatActivity {

    Button nameButton;
    TextView nameTextView;
    EditText nameEditText;
    IMC imc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nameButton = (Button) findViewById(R.id.nameButton);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        nameEditText = (EditText) findViewById(R.id.nameEditText);

        imc = new IMC();

        nameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validationField();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void openGenderActivity(){
        Intent intent = new Intent(this, GenderActivity.class);
        intent.putExtra("imc", imc);
        startActivity(intent);
    }

    public void validationField(){
        if(nameEditText.getText().length() == 0){
            Toast.makeText(getApplicationContext(), "Nome vazio!", Toast.LENGTH_SHORT).show();
        }else{
            imc.setName(nameEditText.getText().toString());
            openGenderActivity();
        }
    }
}
