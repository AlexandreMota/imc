package com.alexandremota.imc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.alexandremota.imc.entity.IMC;

public class GenderActivity extends AppCompatActivity {

    IMC imc;
    Button genderButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imc = (IMC) getIntent().getExtras().getSerializable("imc");

        genderButton = (Button) findViewById(R.id.genderButton);

        genderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openHeightActivity();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.manRadioButton:
                if (checked)
                    imc.setGender("Masculino");
                    break;
            case R.id.womanRadioButton:
                if (checked)
                   imc.setGender("Feminino");
                    break;
        }
    }

    public void openHeightActivity(){
        Intent intent = new Intent(this, HeightActivity.class);
        intent.putExtra("imc", imc);
        startActivity(intent);
    }
}
