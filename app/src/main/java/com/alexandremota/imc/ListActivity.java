package com.alexandremota.imc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.mukesh.tinydb.TinyDB;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listView = (ListView) findViewById(R.id.listView);

        //gravar();
        initValues();

    }


    public void initValues(){
        TinyDB tinyDB = new TinyDB(getApplicationContext());

        ArrayList<String> list = tinyDB.getListString("gravados");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, list);

        listView.setAdapter(adapter);
    }


    public void gravar(){
       String[] FRUITS = new String[] { "Apple", "Avocado", "Banana",
                "Blueberry", "Coconut", "Durian", "Guava", "Kiwifruit",
                "Jackfruit", "Mango", "Olive", "Pear", "Sugar-apple" };

        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < FRUITS.length; ++i) {
            list.add(FRUITS[i]);
        }

        TinyDB tinyDB = new TinyDB(getApplicationContext());
        tinyDB.putListString("gravados", list);
    }
}
