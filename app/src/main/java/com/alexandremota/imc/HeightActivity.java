package com.alexandremota.imc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alexandremota.imc.entity.IMC;

public class HeightActivity extends AppCompatActivity {
    IMC imc;
    Button heightButton;
    EditText heightEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_height);
        imc = (IMC) getIntent().getExtras().getSerializable("imc");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        heightEditText = (EditText) findViewById(R.id.heightEditText);
        heightButton = (Button) findViewById(R.id.heightButton);

        heightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validationField();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void openWeightActivity(){
        Intent intent = new Intent(this, WeightActivity.class);
        intent.putExtra("imc", imc);
        startActivity(intent);
    }

    public void validationField(){
        if(heightEditText.getText().length() == 0){
            Toast.makeText(getApplicationContext(), "Altura vazia!", Toast.LENGTH_SHORT).show();
        }else{
            imc.setHeight(Double.parseDouble(heightEditText.getText().toString()));
            openWeightActivity();
        }
    }
}
