package com.alexandremota.imc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alexandremota.imc.entity.IMC;
import com.mukesh.tinydb.TinyDB;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    IMC imc;
    TextView imcTextView, genderTextView, valueTextView, statusTextView, nameTextView;

   Button resultButton, shareButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        imcTextView = (TextView) findViewById(R.id.imcTextView);
        genderTextView = (TextView) findViewById(R.id.genderTextView);
        valueTextView = (TextView) findViewById(R.id.valueTextView);
        statusTextView = (TextView) findViewById(R.id.statusTextView);
        nameTextView = (TextView) findViewById(R.id.nameTextView);
        resultButton = (Button) findViewById(R.id.resultButton);
        shareButton = (Button) findViewById(R.id.shareButton);

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                socialShare();
            }
        });

        resultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivity();
            }
        });

        imc = (IMC) getIntent().getExtras().getSerializable("imc");
        initValues();
        saveIMC();
    }

    public void initValues(){
        genderTextView.setText(imc.getGender());
        valueTextView.setText(imc.calculatorValueIMC()+"");
        statusTextView.setText(imc.calculatorStatusIMC());
        nameTextView.setText(imc.getName());
    }

    public void saveIMC(){
        TinyDB tinyDB = new TinyDB(getApplicationContext());
        ArrayList<String> list = tinyDB.getListString("gravados");
        list.add(imc.getName() +" : "+imc.calculatorStatusIMC());
        tinyDB.putListString("gravados", list);

    }

    public void openMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void socialShare(){
        String message = imc.getName()+": "+imc.calculatorStatusIMC();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);

        startActivity(Intent.createChooser(share, "Compartilhando seu IMC"));
    }
}
