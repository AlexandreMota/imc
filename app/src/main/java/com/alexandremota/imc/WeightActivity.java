package com.alexandremota.imc;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alexandremota.imc.entity.IMC;

public class WeightActivity extends AppCompatActivity {

    IMC imc;
    Button weightButton;
    EditText weightEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imc = (IMC) getIntent().getExtras().getSerializable("imc");
        weightButton = (Button) findViewById(R.id.weightButton);
        weightEditText = (EditText) findViewById(R.id.weightEditText);

        weightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validationField();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public void openResultActivity(){
        Intent intent = new Intent(this, ResultActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("imc", imc);
        startActivity(intent);
    }

    public void validationField(){
        if(weightEditText.getText().length() == 0){
            Toast.makeText(getApplicationContext(), "Peso vazio!", Toast.LENGTH_SHORT).show();
        }else{

            new AlertDialog.Builder(this)
                    .setTitle(R.string.confirmation)
                    .setMessage(R.string.info_confirmation)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Stop the activity
                            imc.setWeight(Double.parseDouble(weightEditText.getText().toString()));
                            openResultActivity();
                            //WeightActivity.this.finish();
                        }

                    })
                    .setNegativeButton(R.string.no, null)
                    .show();


        }
    }
}
